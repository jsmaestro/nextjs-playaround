import { PropsWithChildren, createContext, useContext } from "react";
import useSessionStorage from "../hooks/useSessionStorage";

export const SESSION_USER_KEY = "sessionUserId";

type User = string;
type AuthContext = {
  user: User | null;
  isAuthenticated: boolean;
  signInUser: (user: User) => void;
  signOutUser: () => void;
};

const defaultContext = {
  user: null,
  isAuthenticated: false,
  signInUser: (_: User) => {},
  signOutUser: () => {}
};

export const AuthStore = createContext<AuthContext>(defaultContext);

export function AuthContextProvider({ children }: PropsWithChildren) {
  const [user, setUser] = useSessionStorage<User>(SESSION_USER_KEY, "", {});

  const contextValue: AuthContext = {
    user,
    signInUser: setUser,
    isAuthenticated: !!user,
    signOutUser: () => sessionStorage.removeItem(SESSION_USER_KEY)
  };

  return (
    <AuthStore.Provider value={contextValue}>{children}</AuthStore.Provider>
  );
}

export function useAuth() {
  return useContext(AuthStore);
}
