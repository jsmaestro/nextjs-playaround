import { useEffect, useState } from "react";
import { Container, Typography } from "@mui/material";
import { useAuth } from "@/context/auth";
import PostCard, { Post } from "@/components/post";

export default function Home() {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<Error | null>(null);
  const [posts, setPosts] = useState<Post[]>([]);

  const { isAuthenticated } = useAuth();

  useEffect(() => {
    (async function fetchData() {
      if (!isAuthenticated) return;
      setLoading(true);
      try {
        const response = await fetch(
          process.env.NEXT_PUBLIC_ENDPOINT as string
        );
        const data = await response.json();

        setPosts(data);
      } catch (error) {
        setPosts([]);
        setError(new Error("Something went wrong"));
        console.error(error);
      } finally {
        setLoading(false);
      }
    })();
  }, [isAuthenticated]);

  return (
    <Container
      sx={{
        display: "grid",
        gridTemplateColumns: "repeat(auto-fill, minmax(20rem, 1fr))",
        gap: 3
      }}
    >
      {loading && "loading..."}
      {error && (
        <Typography variant="body2" color="alert">
          {error.message}
        </Typography>
      )}
      {posts.map((post) => (
        <PostCard key={post.id} post={post} />
      ))}
    </Container>
  );
}
