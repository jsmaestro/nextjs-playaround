import { useCallback, useEffect, useState } from "react";
import getStorageArea from "./utils/getStorageArea";

type ValueSetter<T> = (currentValue: T) => T;

function useStorage<T>(
  storageType: "localStorage" | "sessionStorage",
  key: string,
  initialValue: T,
  options: {
    persistInitialValue?: boolean;
    reviver?: (key: string, value: any) => any;
  } = {}
): [T, (valueOrSetter: T | ValueSetter<T>) => void] {
  const storageArea = getStorageArea(storageType);

  const getCurrentValue = useCallback(() => {
    if (!storageArea) {
      return null;
    }

    const rawValue = storageArea.getItem(key);

    if (!rawValue) {
      return null;
    }

    return JSON.parse(rawValue, options.reviver);
  }, [key, options.reviver, storageArea]);

  const [storedValue, setStoredValue] = useState<T>(() => {
    try {
      const currentValue = getCurrentValue();
      return currentValue ?? initialValue;
    } catch (error) {
      console.error(error);
      return initialValue;
    }
  });

  useEffect(() => {
    if (!storageArea) {
      return;
    }

    // optionally persist the initial value, depending on supplied options
    if (options.persistInitialValue) {
      const item = storageArea.getItem(key);
      if (!item) {
        storageArea.setItem(key, JSON.stringify(initialValue));
      }
    }
  }, [initialValue, storageArea, key, options.persistInitialValue]);

  useEffect(() => {
    const listener = (event: StorageEvent) => {
      if (event.key === key && event.storageArea === storageArea) {
        setStoredValue(
          event.newValue
            ? JSON.parse(event.newValue, options.reviver)
            : event.newValue
        );
      }
    };

    window.addEventListener("storage", listener);

    return () => {
      window.removeEventListener("storage", listener);
    };
  }, [key, options.reviver, storageArea]);

  const wrappedSetter = useCallback(
    (valueOrSetter: T | ValueSetter<T>) => {
      if (!storageArea) {
        return;
      }

      const currentValue = getCurrentValue();

      const valueToStore =
        valueOrSetter instanceof Function // Allow value to be a function so we have same API as useState
          ? valueOrSetter(currentValue || initialValue)
          : valueOrSetter;

      setStoredValue(valueToStore);

      try {
        storageArea.setItem(key, JSON.stringify(valueToStore));

        /*
          The StorageEvent is dispatched to other documents by default which means we can't listen for it
          within the same document. To workaround this we can dispatch a custom storage event which will
          then hit our listener (above) to ensure all instances of useStorage with the same key stay in sync.

          https://developer.mozilla.org/en-US/docs/Web/API/StorageEvent
          https://stackoverflow.com/questions/18090521/forcing-local-storage-events-to-fire-in-the-same-window
        */
        window.dispatchEvent(
          new StorageEvent("storage", {
            key,
            oldValue: JSON.stringify(currentValue),
            newValue: JSON.stringify(valueToStore),
            storageArea
          })
        );
      } catch (error) {
        console.error(error);
      }
    },
    [key, getCurrentValue, initialValue, storageArea]
  );

  return [storedValue, wrappedSetter];
}

export default useStorage;
