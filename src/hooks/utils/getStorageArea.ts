/**
 * Safely wraps the fetching of browser storage areas and either returns the storage area or undefined
 * if it cannot be accessed.
 *
 * Will safely run in server-side code and when a user blocks cookies in their browser.
 *
 * @param storageType The type of storage area to fetch. Either `localStorage` or `sessionStorage`.
 * @returns The storage area or `undefined` if not available.
 */
const getStorageArea = (storageType: "localStorage" | "sessionStorage") => {
  if (typeof window === "undefined") {
    return undefined;
  }

  let storageArea: Storage | undefined;

  try {
    storageArea = window[storageType];
  } catch {
    storageArea = undefined;
  }

  return storageArea;
};

export default getStorageArea;
