import useStorage from "./useStorage";

type ValueSetter<T> = (currentValue: T) => T;

function useSessionStorage<T>(
  key: string,
  initialValue: T,
  options: {
    persistInitialValue?: boolean;
    reviver?: (key: string, value: any) => any;
  } = {}
): [T, (valueOrSetter: T | ValueSetter<T>) => void] {
  return useStorage("sessionStorage", key, initialValue, options);
}

export default useSessionStorage;
