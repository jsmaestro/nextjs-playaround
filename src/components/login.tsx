"use client";

import { FormEvent, useEffect, useState } from "react";
import { useRouter } from "next/router";

import { LockOutlined } from "@mui/icons-material";
import {
  Avatar,
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  Link,
  Paper,
  TextField,
  Typography
} from "@mui/material";
import { useAuth } from "@/context/auth";

export default function Login() {
  const router = useRouter();
  const [username, setUserName] = useState("");

  const { isAuthenticated, signInUser } = useAuth();

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    signInUser(username);
  };

  return (
    <Grid display="flex" alignItems="center" height="100%">
      <Paper
        elevation={10}
        sx={{
          padding: 3,
          height: "70vh",
          width: ["100%", 640],
          margin: "20px auto"
        }}
      >
        <form onSubmit={handleSubmit}>
          <Grid
            display="flex"
            flexDirection="column"
            marginBottom={2}
            alignItems="center"
          >
            <Avatar sx={{ backgroundColor: "#1bbd7e", marginBottom: 1 }}>
              <LockOutlined />
            </Avatar>
            <h2>Sign In</h2>
          </Grid>
          <TextField
            label="Username"
            placeholder="Enter username"
            variant="outlined"
            fullWidth
            required
            margin="normal"
            onChange={(e) => setUserName(e.target.value)}
          />
          <TextField
            disabled
            label="Password"
            placeholder="Enter password"
            type="password"
            variant="outlined"
            fullWidth
            required
            margin="normal"
          />
          <FormControlLabel
            control={<Checkbox name="checkedB" color="primary" disabled />}
            label="Remember me"
          />
          <Button
            type="submit"
            color="primary"
            variant="contained"
            fullWidth
            sx={{ marginTop: 2, marginBottom: 2 }}
          >
            Sign in
          </Button>
        </form>
        <Typography marginBottom={1}>
          <Link href="#">Forgot password?</Link>
        </Typography>
        <Typography>
          Do you have an account? <Link href="#">Sign Up</Link>
        </Typography>
      </Paper>
    </Grid>
  );
}
