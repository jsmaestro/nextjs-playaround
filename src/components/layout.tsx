import Head from "next/head";
import { PropsWithChildren, useEffect } from "react";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import { useAuth } from "@/context/auth";
import { useRouter } from "next/router";
import Login from "./login";

const inter = Inter({ subsets: ["latin"] });

export default function Layout({ children }: PropsWithChildren) {
  const { isAuthenticated, user, signOutUser } = useAuth();
  const router = useRouter();

  const handleSignOut = () => {
    signOutUser();
    router.reload();
  };

  return (
    <>
      <Head>
        <title>Code Challenge</title>
        <meta name="description" content="Next.js Typescript Material UI" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={`${styles.main} ${inter.className}`}>
        {isAuthenticated && (
          <nav className={styles.navigation}>
            <div className={styles.description}>Hello {user}</div>
            <a href="#" onClick={handleSignOut}>
              Logout
            </a>
          </nav>
        )}
        {isAuthenticated ? children : <Login />}
      </main>
    </>
  );
}
