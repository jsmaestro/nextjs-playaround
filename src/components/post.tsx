import { Card, Typography } from "@mui/material";
import { Box } from "@mui/system";

export type Post = {
  userId: number;
  id: number;
  title: string;
  body: string;
};

export default function PostCard({ post }: { post: Post }) {
  const { userId, title, body } = post;
  return (
    <Card sx={{ padding: 2, display: "flex", flexDirection: "column" }}>
      <Typography variant="h5" marginBottom={2}>
        {title}
      </Typography>
      <Typography variant="body1" marginBottom={3}>
        {body}
      </Typography>
      <Box display="flex" justifyContent="space-between" marginTop="auto">
        <Typography variant="caption">User:</Typography>
        <Typography variant="body2">{userId}</Typography>
      </Box>
    </Card>
  );
}
